use eve_toys_settings::PostgresConfig;

pub mod domain;

pub async fn make_pool(
    pg_settings: &PostgresConfig,
) -> Result<sqlx::postgres::PgPool, sqlx::Error> {
    let pg_pool = sqlx::postgres::PgPoolOptions::new()
        .max_connections(5)
        .connect(pg_settings.url.as_str())
        .await?;

    sqlx::migrate!()
        .run(&pg_pool)
        .await
        .expect("Migrations failed");

    Ok(pg_pool)
}
