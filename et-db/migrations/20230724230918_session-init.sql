-- Add migration script here
CREATE TABLE accounts (
    "id" uuid NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
    "last_login" timestamp WITH TIME ZONE NOT NULL
);

CREATE TABLE sessions (
    "id" uuid NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
    "expires" timestamp WITH TIME ZONE NOT NULL,
    "account_id" uuid REFERENCES accounts NOT NULL
);

CREATE TABLE characters (
    "id" integer NOT NULL PRIMARY KEY,
    "name" varchar(40) NOT NULL,
    "account_id" uuid REFERENCES accounts NOT NULL
);

CREATE TABLE character_tokens (
    "character_id" integer REFERENCES characters UNIQUE,
    "access_token" text NOT NULL,
    "expires" timestamp WITH TIME ZONE NOT NULL,
    "token_type" varchar(40) NOT NULL,
    "refresh_token" varchar(40) NOT NULL
);
