-- Add migration script here
CREATE TABLE sde_item_types (
    "id" integer NOT NULL PRIMARY KEY,
    "name" varchar(100) NOT NULL,
    "description" text,
    "icon_id" integer
);
