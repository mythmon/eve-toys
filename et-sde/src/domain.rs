//! This file contains the structs for the YAML files in the SDE.
//!
//! There are a lot of fields in the source files that, for simplicitly, are not
//! represented here. We should add them as needed.

use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct LocalizedString {
    pub en: Option<String>,
}

#[derive(Deserialize, Debug)]
pub struct ItemType {
    pub id: u32,
    pub name: LocalizedString,
    pub published: bool,
    pub description: Option<LocalizedString>,
    #[serde(alias = "iconID")]
    pub icon_id: Option<u32>,
}

impl From<ItemType> for eve_toys_db::domain::SdeItemType {
    fn from(val: ItemType) -> Self {
        eve_toys_db::domain::SdeItemType {
            id: val.id,
            name: val.name.en.unwrap_or_default(),
            description: val.description.and_then(|d| d.en),
            icon_id: val.icon_id,
        }
    }
}
