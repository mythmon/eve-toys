use std::{
    collections::HashMap,
    fs::{self, File},
    io::Write,
};

use anyhow::{Context, Result};
use indicatif::ProgressStyle;
use reqwest::Response;

pub mod domain;

const SDE_DOWNLOAD_URL: &str =
    "https://eve-static-data-export.s3-eu-west-1.amazonaws.com/tranquility/sde.zip";
const SDE_CHECKSUM_URL: &str =
    "https://eve-static-data-export.s3-eu-west-1.amazonaws.com/tranquility/checksum";
const SDE_LOCAL_PATH: &str = ".cache/sde.zip";
const SDE_LOCAL_CHECKSUM_PATH: &str = ".cache/sde_checksum";

#[tokio::main]
async fn main() -> Result<()> {
    download_sde().await?;
    load_data().await?;
    Ok(())
}

async fn download_sde() -> Result<()> {
    let res = reqwest::get(SDE_CHECKSUM_URL)
        .await
        .and_then(Response::error_for_status)?;
    let remote_checksum = res.text().await?;
    let remote_checksum = remote_checksum.trim();

    if let Ok(_) = File::open(SDE_LOCAL_PATH) {
        if let Ok(local_checksum) = fs::read_to_string(SDE_LOCAL_CHECKSUM_PATH) {
            let local_checksum = local_checksum.trim();
            if local_checksum == remote_checksum {
                println!("Using cached SDE");
                return Ok(());
            }
        }
    };

    println!("Downloading SDE data");
    fs::create_dir_all("./.cache").context("making cache directory")?;
    let mut res = reqwest::get(SDE_DOWNLOAD_URL)
        .await
        .and_then(Response::error_for_status)?;
    let mut temp = tempfile::NamedTempFile::new()?;

    let progress_style = ProgressStyle::default_bar()
        .template("{spinner:.green} [{elapsed}/{duration}] [{wide_bar}] {bytes}/{total_bytes}")?;
    let progress_bar =
        indicatif::ProgressBar::new(res.content_length().unwrap_or(0)).with_style(progress_style);

    while let Some(chunk) = res.chunk().await? {
        temp.write_all(&chunk)?;
        progress_bar.inc(chunk.len() as u64);
    }

    fs::rename(&temp, SDE_LOCAL_PATH).or_else(|err| {
        fs::copy(&temp, SDE_LOCAL_PATH)
            .context(format!("Couldn't rename: {err}"))
            .map(|_| ())
    })?;
    fs::write(SDE_LOCAL_CHECKSUM_PATH, remote_checksum)?;

    Ok(())
}

async fn load_data() -> Result<()> {
    let settings = eve_toys_settings::Settings::load()?;
    let pool = eve_toys_db::make_pool(&settings.postgres)
        .await
        .context("Failed to make database pool")?;

    let mut file = fs::File::open(SDE_LOCAL_PATH)?;
    let mut zip_file = zip::read::ZipArchive::new(&mut file)?;
    let file = zip_file.by_name("sde/fsd/typeIDs.yaml")?;

    // This could probably be done in a more bulk or streaming way, but this is fast enough.
    println!("Loading data");
    let item_types: Vec<domain::ItemType> = {
        let raw_data: HashMap<u32, serde_yaml::Mapping> = serde_yaml::from_reader(file)?;
        raw_data
            .into_iter()
            .map(|(id, mut item)| {
                item.insert("id".into(), id.into());
                match serde_yaml::from_value(item.into()) {
                    Ok(item) => Ok(item),
                    Err(error) => {
                        println!("Error parsing item {}: {}", id, error);
                        Err(error)
                    }
                }
            })
            .collect::<Result<Vec<_>, _>>()?
    };

    println!("Inserting data");
    let for_db: Vec<eve_toys_db::domain::SdeItemType> = item_types
        .into_iter()
        .filter(|i| i.published)
        .map(|i| i.into())
        .collect();

    sqlx::query(
        r#"
        INSERT INTO sde_item_types (id, name, description, icon_id)
        (SELECT * FROM UNNEST($1::integer[], $2::varchar[], $3::text[], $4::integer[]))
        ON CONFLICT (id) DO UPDATE SET
            name = EXCLUDED.name,
            description = EXCLUDED.description,
            icon_id = EXCLUDED.icon_id
        "#,
    )
    .bind(&for_db.iter().map(|i| i.id as i64).collect::<Vec<_>>())
    .bind(&for_db.iter().map(|i| i.name.clone()).collect::<Vec<_>>())
    .bind(
        &for_db
            .iter()
            .map(|i| i.description.as_ref().map(|s| s.as_str()))
            .collect::<Vec<_>>(),
    )
    .bind(
        &for_db
            .iter()
            .map(|i| i.icon_id.map(|v| v as i64))
            .collect::<Vec<_>>(),
    )
    .execute(&pool)
    .await?;

    Ok(())
}
