set windows-shell := ["powershell.exe", "-NoLogo", "-Command"]

_default:
  just --list

build:
  docker build --target app --tag eve-toys .

deploy: build
  fly deploy --local-only --config .fly/web.toml

dev:
  cargo watch -x run

dependencies:
  docker compose up -d

psql: dependencies
  docker compose exec -it --user postgres db psql -d eve-toys

watch +RECIPE:
  watchexec --debounce 1000 --restart -- just {{RECIPE}}

gen-esi-openapi:
  rm -r esi/*
  docker run --rm \
    --volume "${PWD}/esi:/local" \
    openapitools/openapi-generator-cli generate \
    -i https://esi.evetech.net/latest/swagger.json \
    -g rust \
    -o /local \
    -p packageName=esi \
    -p preferUnsignedInt=true \
    -p useSingleRequestParameter=true
  rm esi/git_push.sh 
  rm esi/.travis.yml
