# Eve Toys

A place to write Rust code that works with Eve Online's ESI.

## Web

You'll need to configure ESI auth. Creating a developer application on ESI, and
get the client ID and secret key. Use `http://localhost:3000/auth/esi/callback`
as the callback URL.

To generate the `cookie_key` you can run `cargo run --bin gen_key`.

Then create a configuration file at `./settings.local.toml` with these contents:

```toml
[web]
cookie_key = "<cookie_key>"

[esi]
client_id = "<client_id>"
secret_key = "<secret_key>"

[postgres]
url = "postgres://postgres:postgres@localhost:5432/eve-toys"
```

You can also use other common data formats (such as YAML), or the environment
variables `ET_ESI_CLIENT_ID` and `ET_ESI_SECRET_KEY`.

Then run `cargo run` or other normal Rust commands.

### Migrations

1. `cargo install sqlx-cli`
2. `sqlx migrate add --source .\et-db\migrations\ <description>`

### SDE

You'll need to download the SDE data files from CCP. To do that, run `cargo run --bin load-sde --features=downloader`. This will download about 100MBs, and then insert some data into your database. Future runs will re-use the downloaded zip file. This is idempotent.


### Quick start

1. Go to `https://rustup.rs/` follow instructions
2. Install docker `https://www.docker.com/`
3. Watch paint dry
4. Install just `cargo install just`
5. Create a `settings.local.toml` file add the example above
6. Create your cookie_key `cargo run --bin gen_key` add key to toml file
7. Install watch `cargo install cargo-watch`
8. Add ESI keys to toml from: `https://developers.eveonline.com/applications`
9. Start Postgress `just dependencies`
10. Download SDE (see SDE section)