# syntax = docker/dockerfile:1.4

# =============================================================================
FROM ubuntu:22.04 AS base

SHELL ["/bin/bash", "-c"]

# =============================================================================
FROM base AS builder

# Install compile-time dependencies
RUN --mount=type=cache,target=/var/lib/cache \
  set -eux; \
  apt update; \
  apt install -y --no-install-recommends curl ca-certificates gcc libc6-dev pkg-config libssl-dev;

# Install rustup
RUN set -eux; \
  curl --location --fail \
  "https://static.rust-lang.org/rustup/dist/x86_64-unknown-linux-gnu/rustup-init" \
  --output rustup-init; \
  chmod +x rustup-init; \
  ./rustup-init -y --no-modify-path --default-toolchain stable; \
  rm rustup-init;

# Add rustup to path, check that it works
ENV PATH=${PATH}:/root/.cargo/bin
RUN set -eux; \
  rustup --version; \
  cargo --version; \
  rustc --version;

# Install Rust stable using rustup
RUN \
  --mount=type=cache,target=/root/.rustup \
  --mount=type=cache,target=/root/.cargo/registry \
  --mount=type=cache,target=/root/.cargo/git \
  rustup install stable; \
  rustup default stable;

# # Copy sources and build them
RUN mkdir /app
WORKDIR /app
COPY et-web et-web
COPY et-db et-db
COPY et-sde et-sde
COPY et-settings et-settings
COPY Cargo.toml Cargo.lock ./
RUN \
  --mount=type=cache,target=/root/.rustup \
  --mount=type=cache,target=/root/.cargo/registry \
  --mount=type=cache,target=/root/.cargo/git \
  --mount=type=cache,target=/app/target \
  set -eux; \
  cargo build --release --bin et-web
RUN --mount=type=cache,target=/app/target \
  pwd; \
  objcopy --compress-debug-sections ./target/release/et-web /et-web

# =============================================================================
FROM base AS app

# Install run-time dependencies
RUN --mount=type=cache,target=/var/lib/cache \
  set -eux; \
  apt update; \
  apt install -y --no-install-recommends \
  ca-certificates; \
  apt clean autoclean; \
  apt autoremove --yes --purge; \
  rm -rf /var/lib/{apt,dpkg,log}/

RUN mkdir /app
WORKDIR /app
COPY --from=builder /et-web ./et-web-bin
RUN ls
COPY ./et-web/static ./et-web/static
COPY ./settings.default.toml /app/

WORKDIR /app

CMD ["/app/et-web-bin"]
