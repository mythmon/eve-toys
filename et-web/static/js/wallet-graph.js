import * as Plot from "https://cdn.jsdelivr.net/npm/@observablehq/plot@0.6/+esm";

function getTemplateInfo(id, fn = (d) => d) {
  let el = document.querySelector(`[type="application/json"]#${id}`);
  if (!el) throw new Error(`No json script tag with id ${id}`);
  let data = JSON.parse(el.textContent);
  if (Array.isArray(data)) return data.map(fn);
  return fn(data);
}

function renderChart() {
  const characterInfo = getTemplateInfo("character-info");
  const chartData = getTemplateInfo("running-balance-data", (d) => ({
    ...d,
    character_name: characterInfo[d.character_id].character_name,
    date: new Date(d.date),
  }));
  const plotWidth = getTemplateInfo("plot-width");
  const plotHeight = getTemplateInfo("plot-height");

  const target = document.querySelector("#plot-target");

  const plot = Plot.plot({
    x: { grid: true, type: "utc" },
    y: { tickFormat: "s", grid: true },
    color: { legend: true },
    width: plotWidth,
    // Account for the height and margin, respectively, of the color legend
    height: plotHeight - 33 - 5,
    style: { background: "transparent" },
    marks: [
      Plot.ruleY([0]),
      Plot.areaY(chartData, {
        x: "date",
        y: "balance",
        fill: "character_name",
        curve: "step-after",
      }),
      Plot.lineY(
        chartData,
        Plot.groupX(
          { y: "sum" },
          {
            x: "date",
            y: "balance",
            stroke: "currentcolor",
            strokeWidth: 1,
            curve: "step-after",
            tip: true,
          }
        )
      ),
    ],
  });

  while (target.children.length > 0) {
    target.removeChild(target.children[0]);
  }
  target.append(plot);
}

renderChart();

// Re-render the chart after each HTMX swap. Because this script file exists
// before and after the swap, it isn't re-run automatically.
document.addEventListener("htmx:afterSettle", (ev) => renderChart());
