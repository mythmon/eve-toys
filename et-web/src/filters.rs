use std::{borrow::Borrow, fmt::Display};

use askama::{filters::safe, Html};
use time::{format_description, macros::offset, OffsetDateTime};

pub trait Displayable {
    type Output: Display;
    fn into_displayable(self) -> Self::Output;
}

impl<T: Display> Displayable for T {
    type Output = Self;
    fn into_displayable(self) -> Self::Output {
        self
    }
}

pub trait OptionDisplayable {
    type Output: Display;
    fn into_option_displayable(self) -> Option<Self::Output>;
}

impl<T: Display> OptionDisplayable for Option<T> {
    type Output = T;
    fn into_option_displayable(self) -> Option<Self::Output> {
        self
    }
}

impl<'a, T: Display> OptionDisplayable for &'a Option<T> {
    type Output = &'a T;
    fn into_option_displayable(self) -> Option<Self::Output> {
        self.as_ref()
    }
}

/// # Errors
/// Infallible
pub fn or<T1, T2>(a: T1, b: T2) -> askama::Result<impl Display>
where
    T1: OptionDisplayable,
    T2: Displayable,
{
    let a = a.into_option_displayable();
    let b = b.into_displayable();
    Ok(a.map_or_else(|| format!("{b}"), |v| format!("{v}")))
}

/// # Errors
/// Infallible
pub fn opt_eq<T1, T2>(a: T1, b: T2) -> askama::Result<bool>
where
    T1: OptionDisplayable,
    T2: Displayable,
{
    let a = a.into_option_displayable();
    let b = b.into_displayable();
    Ok(match a {
        Some(a) => a.to_string() == b.to_string(),
        None => false,
    })
}

/// # Errors
/// If the dates don't render correctly
pub fn eve_date_time<D: Borrow<OffsetDateTime>>(dt: D) -> askama::Result<impl Display> {
    let dt = dt.borrow().to_offset(offset!(UTC));
    let date_format = format_description::parse("[year]-[month]-[day]")
        .map_err(|e| askama::Error::Custom(Box::new(e)))?;
    let time_format = format_description::parse("[hour]:[minute]")
        .map_err(|e| askama::Error::Custom(Box::new(e)))?;

    let date_str = dt
        .format(&date_format)
        .map_err(|e| askama::Error::Custom(Box::new(e)))?;
    let time_str = dt
        .format(&time_format)
        .map_err(|e| askama::Error::Custom(Box::new(e)))?;
    let html = format!(
        r#"<timestamp class="dt">
                <span class="dt-date">{date_str}</span>
                <span class="dt-time">{time_str}</span>
            </timestamp>"#
    );
    safe(Html, html)
}
