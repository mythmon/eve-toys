use anyhow::anyhow;
use axum::response::IntoResponse;
use std::error::Error as StdError;
use thiserror::Error;

#[derive(Debug, Error)]
#[allow(clippy::module_name_repetitions)]
pub enum ETError {
    #[error("Unknown error: {0:?}")]
    SomethingWentWrong(#[from] anyhow::Error),

    #[error("Auth error: {0:?}")]
    Auth(#[source] anyhow::Error),

    #[error("Deserialization error: {0:?}")]
    Deserialization(#[source] anyhow::Error),

    #[error("IO error: {0:?}")]
    Io(#[source] std::io::Error),

    #[error("Error loading settings: {0:?}")]
    Settings(#[source] anyhow::Error),

    #[error("Error with database query: {0:?}")]
    Db(#[source] anyhow::Error),

    #[error("Error with header format: {0:?}")]
    Header(#[from] axum::http::header::InvalidHeaderValue),

    #[error("Error with URL format: {0:?}")]
    Url(#[from] url::ParseError),

    #[error("Error with ESI call (reqwest): {0:?}")]
    EsiReqwest(reqwest::Error),

    #[error("Error with ESI call (reqwest): {0:?}")]
    EsiResponse(anyhow::Error),
}

impl ETError {
    pub fn map<E: StdError + Sync + Send + 'static>(err: E) -> Self {
        tracing::error!(?err, "ETError");
        Self::SomethingWentWrong(anyhow::anyhow!(err))
    }
}

impl IntoResponse for ETError {
    fn into_response(self) -> axum::response::Response {
        let body = format!("{self}");
        (axum::http::StatusCode::INTERNAL_SERVER_ERROR, body).into_response()
    }
}

impl<E> From<esi::apis::Error<E>> for ETError
where
    E: std::fmt::Debug,
{
    fn from(value: esi::apis::Error<E>) -> Self {
        match value {
            esi::apis::Error::Reqwest(err) => Self::EsiReqwest(err),
            esi::apis::Error::Serde(err) => Self::Deserialization(err.into()),
            esi::apis::Error::Io(err) => Self::Io(err),
            esi::apis::Error::ResponseError(response_content) => {
                Self::EsiResponse(anyhow!("{:?}", response_content))
            }
        }
    }
}
