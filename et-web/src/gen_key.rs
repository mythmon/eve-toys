use axum_extra::extract::cookie::Key;
use base64::{engine::general_purpose::STANDARD_NO_PAD as b64, Engine};

fn main() {
    let key = Key::generate();
    let encoded = b64.encode(key.master());
    println!("{}", encoded);
}
