#![warn(clippy::all, clippy::pedantic)]
#![allow(clippy::cast_sign_loss, clippy::cast_lossless, clippy::unused_async)]

use axum::{middleware, routing::get, Router};
use tower_http::{compression::CompressionLayer, services::ServeDir, trace::TraceLayer};
use tracing::Level;

mod auth;
mod cache;
mod context;
mod endpoints;
mod error;
mod eve;
mod extractors;
pub mod filters;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt()
        .with_target(false)
        .compact()
        .init();

    let state = context::AppContext::new().await?;

    let app = Router::new()
        .nest_service("/static", ServeDir::new(&state.settings.web.static_dir))
        .route("/_status", get(|| async { "OK" }))
        .route("/", get(endpoints::index::endpoint))
        .route("/wallet-graph", get(endpoints::wallet_graph::page))
        .route("/wallet-graph/chart", get(endpoints::wallet_graph::chart))
        .route("/market-history", get(endpoints::market_history::page))
        .route(
            "/market-history/table",
            get(endpoints::market_history::table),
        )
        .route("/auth/signin", get(auth::signin))
        .route("/auth/esi/callback", get(auth::callback))
        .with_state(state.clone())
        .layer(CompressionLayer::new())
        .layer(middleware::from_fn_with_state(
            state.clone(),
            eve::middleware,
        ))
        .layer(middleware::from_fn_with_state(
            state.clone(),
            auth::middleware,
        ))
        .layer(
            TraceLayer::new_for_http()
                .make_span_with(tower_http::trace::DefaultMakeSpan::new().level(Level::INFO))
                .on_response(tower_http::trace::DefaultOnResponse::new().level(Level::INFO)),
        );

    let sock_addr = state.settings.web.listen;
    tracing::info!("listening on http://{sock_addr}");

    axum::Server::bind(&sock_addr)
        .serve(app.into_make_service())
        .await?;

    Ok(())
}
