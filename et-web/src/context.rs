use crate::{cache::ETCache, error::ETError};
use anyhow::Context;
use axum::extract::FromRef;
use axum_extra::extract::cookie::Key;
use eve_toys_settings::Settings;
use sqlx::PgPool;
use std::{ops::Deref, sync::Arc};

#[derive(Clone)]
pub struct AppContextInner {
    pub cache: ETCache,
    pub http_client: reqwest::Client,
    pub settings: Settings,
    pub pg_pool: PgPool,
}

#[derive(Clone)]
#[allow(clippy::module_name_repetitions)]
pub struct AppContext(Arc<AppContextInner>);

impl AppContext {
    pub async fn new() -> Result<Self, ETError> {
        let settings = Settings::load()
            .context("loading settings")
            .map_err(ETError::Settings)?;

        let pg_pool = eve_toys_db::make_pool(&settings.postgres)
            .await
            .context("Connecting to database")
            .map_err(ETError::Settings)?;

        Ok(Self(Arc::new(AppContextInner {
            cache: ETCache::default(),
            http_client: reqwest::Client::new(),
            settings,
            pg_pool,
        })))
    }
}

impl Deref for AppContext {
    type Target = AppContextInner;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl FromRef<AppContext> for Key {
    fn from_ref(input: &AppContext) -> Self {
        input.settings.web.cookie_key.clone()
    }
}
