use std::ops::Deref;

use base64::{engine::general_purpose::STANDARD_NO_PAD as b64, Engine};
use sha2::{Digest, Sha256};

use crate::auth::CharacterInfo;

#[derive(Clone)]
#[allow(clippy::module_name_repetitions)]
pub struct ETCache {
    pub cache: moka::future::Cache<String, (Expire, serde_json::Value)>,
}

impl Default for ETCache {
    fn default() -> Self {
        let eviction_listener = |key, _value, cause| {
            tracing::info!("Evicted key '{key}' from cache due to {cause:?}");
        };

        let cache = moka::future::Cache::builder()
            .max_capacity(1_000) // TODO max size in bytes instead of entries
            .expire_after(ExpiryPolicy)
            .eviction_listener_with_queued_delivery_mode(eviction_listener)
            .build();
        Self { cache }
    }
}

impl Deref for ETCache {
    type Target = moka::future::Cache<String, (Expire, serde_json::Value)>;

    fn deref(&self) -> &Self::Target {
        &self.cache
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Expire(pub std::time::Duration);

impl Expire {
    pub fn minutes(minutes: u64) -> Self {
        Self(std::time::Duration::from_secs(minutes * 60))
    }
}

struct ExpiryPolicy;

impl<K, V> moka::Expiry<K, (Expire, V)> for ExpiryPolicy {
    fn expire_after_create(
        &self,
        _key: &K,
        value: &(Expire, V),
        _current_time: std::time::Instant,
    ) -> Option<std::time::Duration> {
        Some(value.0 .0)
    }
}

pub fn make_cache_key<K: IntoCacheKey>(key: K) -> String {
    let mut hasher = Sha256::new();
    for part in key.key_parts() {
        hasher.update(part);
    }
    let hash = hasher.finalize();
    b64.encode(hash.as_slice())
}

pub trait IntoCacheKey {
    fn key_parts(self) -> Vec<Vec<u8>>;
}

impl IntoCacheKey for &str {
    fn key_parts(self) -> Vec<Vec<u8>> {
        vec![self.bytes().collect()]
    }
}

impl IntoCacheKey for &String {
    fn key_parts(self) -> Vec<Vec<u8>> {
        vec![self.bytes().collect()]
    }
}

impl IntoCacheKey for u32 {
    fn key_parts(self) -> Vec<Vec<u8>> {
        vec![self.to_be_bytes().to_vec()]
    }
}

impl IntoCacheKey for &CharacterInfo {
    fn key_parts(self) -> Vec<Vec<u8>> {
        self.character_id.key_parts()
    }
}

// TODO set up a macro for implementing IntoCacheKey for tuples
impl<A, B> IntoCacheKey for (A, B)
where
    A: IntoCacheKey,
    B: IntoCacheKey,
{
    fn key_parts(self) -> Vec<Vec<u8>> {
        self.0
            .key_parts()
            .into_iter()
            .chain(self.1.key_parts())
            .collect()
    }
}
