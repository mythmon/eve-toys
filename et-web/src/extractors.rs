use crate::error::ETError;
use axum::{extract::FromRequest, http::Request};
use std::collections::HashMap;

#[derive(Debug, Default, Clone)]
pub struct MultiQueryString(pub HashMap<String, Vec<String>>);

#[axum::async_trait]
impl<S, B> FromRequest<S, B> for MultiQueryString
where
    B: Send + 'static,
    S: Send + Sync,
{
    type Rejection = ETError;

    async fn from_request(req: Request<B>, _state: &S) -> Result<Self, Self::Rejection> {
        let map = req
            .uri()
            .query()
            .map(|mut q| {
                let mut map: HashMap<String, Vec<String>> = HashMap::new();
                if q.starts_with('?') {
                    q = &q[1..];
                }
                for part in q.split('&') {
                    if let Some((key, value)) = part.split_once('=') {
                        map.entry(key.to_string())
                            .or_default()
                            .push(value.to_string());
                    }
                }
                map
            })
            .unwrap_or_default();
        Ok(Self(map))
    }
}
