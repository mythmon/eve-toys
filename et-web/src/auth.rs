use std::sync::Arc;

use crate::{context::AppContext, error::ETError};
use anyhow::anyhow;
use axum::{
    extract::{Query, State},
    http::Request,
    middleware::Next,
    response::{IntoResponse, Redirect},
};
use axum_extra::extract::{cookie::Cookie, PrivateCookieJar};
use base64::{engine::general_purpose::STANDARD_NO_PAD as b64, Engine};
use rand::{thread_rng, Rng};
use serde::{Deserialize, Serialize};
use sqlx::{pool::PoolConnection, types::Uuid, Postgres};
use time::{ext::NumericalDuration, Duration, OffsetDateTime};
use url::Url;

const REQUESTED_SCOPES: &str =
    r#"publicData esi-wallet.read_character_wallet.v1 esi-markets.read_character_orders.v1"#;

pub async fn signin(State(state): State<AppContext>) -> impl IntoResponse {
    let mut location = Url::parse("https://login.eveonline.com/v2/oauth/authorize").unwrap();
    let mut rng = thread_rng();
    let nonce = b64.encode(rng.gen::<[u8; 32]>());
    location
        .query_pairs_mut()
        .append_pair("response_type", "code")
        .append_pair(
            "redirect_uri",
            state
                .settings
                .web
                .base_url
                .join("auth/esi/callback")
                .unwrap()
                .as_str(),
        )
        .append_pair("client_id", &state.settings.esi.client_id)
        .append_pair("scope", REQUESTED_SCOPES)
        .append_pair("state", &nonce);

    Redirect::temporary(location.as_ref())
}

#[derive(Debug, Deserialize)]
pub struct CallbackQuery {
    pub code: String,
    pub state: String,
}

#[derive(Debug, Deserialize, Serialize)]
struct EsiTokenResponse {
    access_token: String,
    expires_in: u32,
    token_type: String,
    refresh_token: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct CharacterInfo {
    #[serde(alias = "CharacterID")]
    pub character_id: u32,
    #[serde(alias = "CharacterName")]
    pub character_name: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct CharacterTokens {
    pub access_token: String,
    expires_at: OffsetDateTime,
    token_type: String,
    refresh_token: String,
}

impl From<EsiTokenResponse> for CharacterTokens {
    fn from(res: EsiTokenResponse) -> Self {
        Self {
            access_token: res.access_token,
            expires_at: OffsetDateTime::now_utc() + Duration::seconds(res.expires_in as i64),
            token_type: res.token_type,
            refresh_token: res.refresh_token,
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct CharacterWithTokens {
    pub info: CharacterInfo,
    pub tokens: CharacterTokens,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
#[allow(clippy::module_name_repetitions)]
pub struct AuthInfo {
    pub chars: Vec<CharacterWithTokens>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
struct SessionCookie {
    id: Uuid,
}

enum GrantType {
    AuthorizationCode,
    RefreshToken,
}

impl GrantType {
    fn as_str(&self) -> &str {
        match self {
            GrantType::AuthorizationCode => "authorization_code",
            GrantType::RefreshToken => "refresh_token",
        }
    }
}

async fn token_endpoint(
    state: &AppContext,
    grant: GrantType,
    code: &str,
) -> Result<CharacterTokens, ETError> {
    let code_param = match grant {
        GrantType::AuthorizationCode => ("code", code),
        GrantType::RefreshToken => ("refresh_token", code),
    };

    let res = state
        .http_client
        .post("https://login.eveonline.com/v2/oauth/token")
        .basic_auth(
            &state.settings.esi.client_id,
            Some(&state.settings.esi.secret_key),
        )
        .form(&[("grant_type", grant.as_str()), code_param])
        .send()
        .await
        .map_err(ETError::map)?;

    // TODO validate body.access_token jwt
    // https://docs.esi.evetech.net/docs/sso/validating_eve_jwt.html

    let new_char = res.json::<EsiTokenResponse>().await.map_err(ETError::map)?;
    Ok(new_char.into())
}

pub async fn callback(
    State(state): State<AppContext>,
    query: Query<CallbackQuery>,
    mut cookies: PrivateCookieJar,
) -> Result<impl IntoResponse, ETError> {
    let char_tokens = token_endpoint(&state, GrantType::AuthorizationCode, &query.code).await?;

    let char_info: CharacterInfo = state
        .http_client
        .get("https://esi.evetech.net/verify")
        .bearer_auth(&char_tokens.access_token)
        .send()
        .await
        .map_err(ETError::map)?
        .error_for_status()
        .map_err(ETError::map)?
        .json()
        .await
        .map_err(ETError::map)?;

    let session_cookie = match cookies.get_json::<SessionCookie>("A") {
        None => None,
        Some(Ok(session_cookie)) => Some(session_cookie),
        Some(Err(err)) => {
            println!("Error loading session_cookie, clearing: {err}");
            cookies = cookies.remove(Cookie::named("A"));
            None
        }
    };

    let connection = state.pg_pool.acquire().await.map_err(ETError::map)?;

    let mut session_store = PostgresSessionStore::from_client(connection);

    // Feels like there should be a clever chain but the await and try make it non-obvious
    let session = if let Some(session_cookie) = session_cookie {
        session_store.load_session(&session_cookie).await?
    } else {
        None
    };

    let session = if let Some(session) = session {
        // Session exists and is valid
        let char_account_id = session_store
            .get_account_id_from_character_id(char_info.character_id)
            .await?;
        if let Some(char_account_id) = char_account_id {
            // Character exists already
            if session.account_id != char_account_id {
                // Session account doesn't match the existing characters account.
                return Err(ETError::Auth(anyhow!(
                    "Character already exists on another account"
                )));
            }
            // Character account and session account match, continue
        } else {
            // Character doesn't exist add to current account
            session_store
                .create_character(&char_info, &session.account_id)
                .await?;
        }
        session
    } else {
        // No session exists, this will also create account and store the character if they're new
        session_store.create_session(&char_info).await?
    };

    session_store
        .update_character_tokens(&char_info, &char_tokens)
        .await?;

    let session_cookie: SessionCookie = session.into();

    let mut cookie = Cookie::new_json("A", session_cookie).map_err(ETError::map)?;
    if let Some(domain) = state.settings.web.base_url.domain() {
        cookie.set_domain(domain.to_string());
    }
    cookie.set_path("/");
    cookie.set_max_age(Some(Duration::WEEK));
    cookie.set_http_only(true);

    Ok((cookies.add(cookie), Redirect::temporary("/")))
}

pub async fn middleware<B: Send>(
    State(state): State<AppContext>,
    mut cookie_jar: PrivateCookieJar,
    mut request: Request<B>,
    next: Next<B>,
) -> Result<impl IntoResponse, ETError> {
    let session_cookie = match cookie_jar.get_json::<SessionCookie>("A") {
        None => None,
        Some(Ok(session_cookie)) => Some(session_cookie),
        Some(Err(err)) => {
            tracing::error!("Error loading session_cookie, clearing: {err}");
            cookie_jar = cookie_jar.remove(Cookie::named("A"));
            None
        }
    };

    let connection = state.pg_pool.acquire().await.map_err(ETError::map)?;
    let mut session_store = PostgresSessionStore::from_client(connection);

    let mut auth_info = if let Some(session_cookie) = &session_cookie {
        session_store
            .auth_info_from_session(session_cookie.id)
            .await?
    } else {
        AuthInfo::default()
    };

    let needs_refresh = auth_info
        .chars
        .iter()
        .filter(|c| c.tokens.expires_at < (OffsetDateTime::now_utc() + 30.seconds()));

    let mut refreshed = false;
    for character in needs_refresh {
        refreshed = true;
        let char_tokens = token_endpoint(
            &state,
            GrantType::RefreshToken,
            &character.tokens.refresh_token,
        )
        .await?;
        session_store
            .update_character_tokens(&character.info, &char_tokens)
            .await?;
    }

    if refreshed {
        if let Some(session_cookie) = &session_cookie {
            auth_info = session_store
                .auth_info_from_session(session_cookie.id)
                .await?;
        }
    }

    request.extensions_mut().insert(Arc::new(Some(auth_info)));
    let response = next.run(request).await;
    Ok((cookie_jar, response).into_response())
}

trait JsonCookieJar: Sized {
    fn get_json<T: serde::de::DeserializeOwned>(
        &self,
        key: &str,
    ) -> Option<Result<T, serde_json::Error>>;
}

trait JsonCookie<'c>: Sized {
    fn new_json<T>(name: &'c str, value: T) -> Result<Self, serde_json::Error>
    where
        T: serde::Serialize;
}

impl JsonCookieJar for PrivateCookieJar {
    fn get_json<T: serde::de::DeserializeOwned>(
        &self,
        key: &str,
    ) -> Option<Result<T, serde_json::Error>> {
        self.get(key)
            .map(|cookie| serde_json::from_str(cookie.value()))
    }
}

impl<'c> JsonCookie<'c> for Cookie<'c> {
    fn new_json<T>(name: &'c str, value: T) -> Result<Self, serde_json::Error>
    where
        T: serde::Serialize,
    {
        let json = serde_json::to_string(&value)?;
        Ok(Self::new(name, json))
    }
}

#[derive(Debug, sqlx::FromRow)]
pub struct Session {
    id: Uuid,
    #[allow(dead_code)]
    expires: OffsetDateTime,
    pub account_id: Uuid,
}

impl From<Session> for SessionCookie {
    fn from(value: Session) -> Self {
        Self { id: value.id }
    }
}

#[derive(Debug)]
pub struct PostgresSessionStore {
    // client: PgPool,
    connection: PoolConnection<Postgres>,
}

impl PostgresSessionStore {
    pub fn from_client(connection: PoolConnection<Postgres>) -> Self {
        Self { connection }
    }
}

/// Database backed session storage
///
/// The `id` field of the `session` table is a `uuid`, which is the value we store in the cookie.
impl PostgresSessionStore {
    /// Given the value of the session cookie, pull the session object out of the db.
    async fn load_session(
        &mut self,
        session_cookie: &SessionCookie,
    ) -> Result<Option<Session>, ETError> {
        sqlx::query_as(
            r#"
            SELECT id, expires, account_id
            FROM sessions
            WHERE id = $1 AND expires > $2"#,
        )
        .bind(session_cookie.id)
        .bind(OffsetDateTime::now_utc())
        .fetch_optional(&mut *self.connection)
        .await
        .map_err(ETError::map)
    }

    fn get_expiration() -> OffsetDateTime {
        OffsetDateTime::now_utc() + Duration::WEEK
    }

    async fn create_session(&mut self, char_info: &CharacterInfo) -> Result<Session, ETError> {
        let account_id = self
            .get_account_id_from_character_id(char_info.character_id)
            .await?;

        let account_id = if let Some(account_id) = account_id {
            account_id
        } else {
            let account_id = self.create_account().await?;
            self.create_character(char_info, &account_id).await?;
            account_id
        };

        sqlx::query_as(
            r#"
            INSERT INTO sessions (expires, account_id)
            VALUES ($1, $2)
            RETURNING id, expires, account_id
            "#,
        )
        .bind(Self::get_expiration())
        .bind(account_id)
        .fetch_one(&mut *self.connection)
        .await
        .map_err(ETError::map)
    }

    async fn update_character_tokens(
        &mut self,
        char_info: &CharacterInfo,
        char_tokens: &CharacterTokens,
    ) -> Result<(), ETError> {
        sqlx::query(
            r#"
            INSERT INTO character_tokens
                (character_id, access_token, expires, token_type, refresh_token)
            VALUES
                ($1, $2, $3, $4, $5)
            ON CONFLICT(character_id) DO UPDATE SET
                access_token = EXCLUDED.access_token,
                expires = EXCLUDED.expires,
                token_type = EXCLUDED.token_type,
                refresh_token = EXCLUDED.refresh_token

        "#,
        )
        .bind(char_info.character_id as i64)
        .bind(&char_tokens.access_token)
        .bind(char_tokens.expires_at)
        .bind(&char_tokens.token_type)
        .bind(&char_tokens.refresh_token)
        .execute(&mut *self.connection)
        .await
        .map_err(ETError::map)?;

        Ok(())
    }

    #[allow(dead_code)]
    async fn destroy_session(&mut self, session: Session) -> Result<(), ETError> {
        // TODO add session list and removal endpoints
        let id = session.id;
        sqlx::query("DELETE FROM sessions WHERE id = $1")
            .bind(id)
            .execute(&mut *self.connection)
            .await
            .map_err(ETError::map)?;

        Ok(())
    }

    async fn get_account_id_from_character_id(
        &mut self,
        character_id: u32,
    ) -> Result<Option<Uuid>, ETError> {
        let account_id: Option<(Uuid,)> = sqlx::query_as(
            r#"
            SELECT account_id FROM characters WHERE id = $1
            "#,
        )
        .bind(character_id as i64)
        .fetch_optional(&mut *self.connection)
        .await
        .map_err(ETError::map)?;

        Ok(account_id.map(|v| v.0))
    }

    async fn create_account(&mut self) -> Result<Uuid, ETError> {
        let (account_id,): (Uuid,) = sqlx::query_as(
            r#"
            INSERT INTO accounts (last_login)
            VALUES ($1)
            RETURNING id"#,
        )
        .bind(OffsetDateTime::now_utc())
        .fetch_one(&mut *self.connection)
        .await
        .map_err(ETError::map)?;

        Ok(account_id)
    }

    async fn create_character(
        &mut self,
        char_info: &CharacterInfo,
        account_id: &Uuid,
    ) -> Result<(), ETError> {
        sqlx::query(
            r#"
            INSERT INTO characters (id, name, account_id)
            VALUES ($1, $2, $3);"#,
        )
        .bind(char_info.character_id as i64)
        .bind(&char_info.character_name)
        .bind(account_id)
        .execute(&mut *self.connection)
        .await
        .map_err(ETError::map)?;

        Ok(())
    }

    async fn auth_info_from_session(&mut self, session_id: Uuid) -> Result<AuthInfo, ETError> {
        // TODO use FromRow magic here
        let results: Vec<(i32, String, String, OffsetDateTime, String, String)> = sqlx::query_as(
            r#"
            SELECT 
                -- CharacterInfo
                c.id, c.name,
                -- CharacterTokens
                ct.access_token, ct.expires, ct.token_type, ct.refresh_token
            FROM character_tokens AS ct
            JOIN characters AS c ON c.id = ct.character_id
            JOIN accounts AS a ON a.id = c.account_id
            JOIN sessions AS s ON s.account_id = a.id
            WHERE s.id = $1
        "#,
        )
        .bind(session_id)
        .fetch_all(&mut *self.connection)
        .await
        .map_err(ETError::map)?;

        let chars = results
            .into_iter()
            .map(
                |(
                    character_id,
                    character_name,
                    access_token,
                    expires_at,
                    token_type,
                    refresh_token,
                )| {
                    CharacterWithTokens {
                        info: CharacterInfo {
                            character_id: character_id as u32,
                            character_name,
                        },
                        tokens: CharacterTokens {
                            access_token,
                            expires_at,
                            token_type,
                            refresh_token,
                        },
                    }
                },
            )
            .collect();

        Ok(AuthInfo { chars })
    }
}
