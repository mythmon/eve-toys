use crate::{
    auth::{AuthInfo, CharacterInfo, CharacterTokens, CharacterWithTokens},
    cache::{make_cache_key, ETCache, Expire, IntoCacheKey},
    context::AppContext,
    error::ETError,
};
use anyhow::{anyhow, Context};
use axum::{extract::State, http::Request, middleware::Next, response::IntoResponse, Extension};
use reqwest::Client;
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use serde_json::{json, Value};
use std::sync::Arc;
use url::Url;

// TODO The auth info is a list of multiple characters. Need a strategy to choose one.

pub struct Api {
    pub auth: Arc<Option<AuthInfo>>,
    base_url: Url,
    cache: ETCache,
    client: Client,
}

pub async fn middleware<B>(
    State(state): State<AppContext>,
    Extension(auth_info): Extension<Arc<Option<AuthInfo>>>,
    mut request: Request<B>,
    next: Next<B>,
) -> Result<impl IntoResponse, ETError> {
    request.extensions_mut().insert(Arc::new(Api {
        auth: auth_info,
        base_url: Url::parse("https://esi.evetech.net/latest").map_err(ETError::map)?,
        cache: state.cache.clone(),
        client: state.http_client.clone(),
    }));
    Ok(next.run(request).await)
}

impl Api {
    async fn cache_request<T: DeserializeOwned, K: IntoCacheKey>(
        &self,
        cache_key: K,
        endpoint: &str,
        auth: Option<&CharacterTokens>,
        expire: Expire,
    ) -> Result<T, ETError> {
        let (_expiry, value) = self
            .cache
            .try_get_with(make_cache_key(cache_key), async {
                let url = self.base_url.join(endpoint).map_err(ETError::map)?;
                let mut req = self.client.get(url);
                if let Some(auth) = auth {
                    req = req.bearer_auth(&auth.access_token);
                }
                let value = req
                    .send()
                    .await
                    .map_err(ETError::map)?
                    .error_for_status()
                    .map_err(ETError::map)?
                    .json()
                    .await
                    .map_err(ETError::map)?;
                Ok((expire, value))
            })
            .await
            .map_err(|err: Arc<ETError>| anyhow!(err))?;

        serde_json::from_value(value).map_err(ETError::map)
    }

    pub fn all_character_info(&self) -> Vec<CharacterInfo> {
        self.auth.as_ref().as_ref().map_or_else(Vec::default, |a| {
            a.chars.iter().map(|c| c.info.clone()).collect()
        })
    }

    pub async fn wallet_transactions(
        &self,
        char_with_tokens: &CharacterWithTokens,
    ) -> Result<Vec<ApiWalletTransaction>, ETError> {
        let journal: Vec<serde_json::Map<String, Value>> = self
            .cache_request(
                (
                    "esi-wallet-transactions",
                    char_with_tokens.info.character_id,
                ),
                &format!(
                    "/latest/characters/{}/wallet/transactions",
                    char_with_tokens.info.character_id
                ),
                Some(&char_with_tokens.tokens),
                Expire::minutes(1),
            )
            .await?;
        let mut rv = journal
            .into_iter()
            .map(|entry| (entry, &char_with_tokens.info).into())
            .collect::<Vec<ApiWalletTransaction>>();
        rv.sort_by_key(|entry| entry.date);
        rv.reverse();
        Ok(rv)
    }

    #[allow(dead_code)]
    pub async fn market_orders(
        &self,
        char_with_tokens: &CharacterWithTokens,
    ) -> Result<Vec<ApiMarketOrder>, ETError> {
        let orders: Vec<serde_json::Map<String, Value>> = self
            .cache_request(
                ("esi-market-orders", &char_with_tokens.info),
                &format!(
                    "latest/characters/{}/orders/history",
                    char_with_tokens.info.character_id
                ),
                Some(&char_with_tokens.tokens),
                Expire::minutes(1),
            )
            .await?;
        let rv = orders
            .into_iter()
            .map(|order| (order, &char_with_tokens.info).into())
            .collect();
        Ok(rv)
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ApiWalletJournalEntry {
    amount: f64,
    balance: f64,
    context_id_type: Option<String>, // probably can make this an enum
    context_id: Option<u64>,
    #[serde(with = "time::serde::iso8601")]
    date: time::OffsetDateTime,
    description: String,
    first_party_id: u64,
    id: u64,
    owner_id: u64,
    reason: String,
    ref_type: String, // probably can make this an enum
    second_party_id: u64,
    tax_receiver_id: Option<u64>,
    tax: Option<f64>,
}

impl TryFrom<(serde_json::Map<String, Value>, &CharacterInfo)> for ApiWalletJournalEntry {
    type Error = ETError;

    fn try_from(
        (mut api_data, character_info): (serde_json::Map<String, Value>, &CharacterInfo),
    ) -> Result<Self, Self::Error> {
        api_data.insert("owner_id".to_string(), json!(character_info.character_id));
        serde_json::from_value(Value::Object(api_data))
            .context("parsing API response for wallet journal")
            .map_err(ETError::Deserialization)
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ApiWalletTransaction {
    pub character_id: u64,
    pub character_name: String,
    pub client_id: u32,
    #[serde(with = "time::serde::iso8601")]
    pub date: time::OffsetDateTime,
    pub is_buy: bool,
    pub is_personal: bool,
    pub journal_ref_id: u64,
    pub location_id: u64,
    pub quantity: u32,
    pub transaction_id: u64,
    pub type_id: u32,
    pub unit_price: f64,
    pub total_price: f64,
}

impl From<(serde_json::Map<String, Value>, &CharacterInfo)> for ApiWalletTransaction {
    fn from(
        (mut api_data, character_info): (serde_json::Map<String, Value>, &CharacterInfo),
    ) -> Self {
        api_data.insert(
            "character_id".to_string(),
            json!(character_info.character_id),
        );
        api_data.insert(
            "character_name".to_string(),
            json!(character_info.character_name),
        );
        api_data.insert(
            "total_price".to_string(),
            json!(
                api_data["unit_price"].as_f64().unwrap() * api_data["quantity"].as_f64().unwrap()
            ),
        );
        serde_json::from_value(Value::Object(api_data)).unwrap()
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ApiMarketOrder {
    // TODO use a real duration?
    pub duration: u32,
    pub is_corporation: bool,
    #[serde(with = "time::serde::iso8601")]
    pub issued: time::OffsetDateTime,
    pub location_id: u64,
    pub order_id: u64,
    pub price: f64,
    pub range: String, // probably can make this an enum
    pub region_id: u64,
    pub state: String, // probably can make this an enum
    pub type_id: u64,
    pub volume_remain: u64,
    pub volume_total: u64,
}

impl From<(serde_json::Map<String, Value>, &CharacterInfo)> for ApiMarketOrder {
    fn from(
        (mut api_data, character_info): (serde_json::Map<String, Value>, &CharacterInfo),
    ) -> Self {
        api_data.insert("owner_id".to_string(), json!(character_info.character_id));
        api_data.insert(
            "owner_name".to_string(),
            json!(character_info.character_name),
        );
        serde_json::from_value(Value::Object(api_data)).unwrap()
    }
}
