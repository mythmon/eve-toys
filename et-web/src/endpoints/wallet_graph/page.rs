use crate::{
    auth::CharacterInfo,
    endpoints::wallet_graph::{PLOT_HEIGHT, PLOT_WIDTH},
    error::ETError,
    eve::Api,
    extractors::MultiQueryString,
};
use anyhow::anyhow;
use askama::Template as AskamaTemplate;
use axum::{
    headers::HeaderValue,
    http::header::HeaderMap,
    response::{Html, IntoResponse},
    Extension,
};
use serde::Serialize;
use std::{collections::HashSet, sync::Arc};

enum Template {
    Page(PageTemplate),
    Content(ContentTemplate),
}

impl Template {
    pub fn render(self) -> Result<String, ETError> {
        match self {
            Template::Page(t) => t.render().map_err(ETError::map),
            Template::Content(t) => t.render().map_err(ETError::map),
        }
    }

    pub fn into_content(self) -> Self {
        match self {
            Template::Page(t) => Template::Content(ContentTemplate {
                characters: t.characters,
                selected_character_ids: t.selected_character_ids,
                plot_height: t.plot_height,
                plot_width: t.plot_width,
            }),
            Template::Content(_) => self,
        }
    }
}

#[derive(Serialize, AskamaTemplate)]
#[template(path = "../templates/wallet_graph/page.html")]
struct PageTemplate {
    characters: Vec<CharacterInfo>,
    selected_character_ids: Vec<u32>,
    plot_width: u32,
    plot_height: u32,
}

#[derive(Serialize, AskamaTemplate)]
#[template(path = "../templates/wallet_graph/content.html")]
struct ContentTemplate {
    characters: Vec<CharacterInfo>,
    selected_character_ids: Vec<u32>,
    plot_width: u32,
    plot_height: u32,
}

#[allow(clippy::unnecessary_wraps)]
mod filters {
    use std::fmt::Display;

    pub fn multi_qs<T: Display>(values: &[T], name: &str) -> askama::Result<impl Display> {
        Ok(values
            .iter()
            .map(|v| format!("{name}={v}"))
            .collect::<Vec<_>>()
            .join("&"))
    }
}

pub async fn endpoint(
    Extension(api): Extension<Arc<Api>>,
    headers: HeaderMap,
    qs: MultiQueryString,
) -> Result<impl IntoResponse, ETError> {
    let all_char_auths = (*api.auth)
        .clone()
        .map(|auth_info| auth_info.chars)
        .unwrap_or_default();

    if all_char_auths.is_empty() {
        Err(ETError::Auth(anyhow!("No characters found")))?;
    }

    let selected_char_ids: HashSet<u32> = match qs.0.get("character_ids") {
        Some(values) => values
            .iter()
            .filter_map(|v| v.parse::<u32>().ok())
            .collect::<HashSet<u32>>(),
        None => vec![all_char_auths[0].info.character_id]
            .into_iter()
            .collect(),
    };

    let characters = all_char_auths.into_iter().map(|c| c.info).collect();
    let selected_character_ids = selected_char_ids.into_iter().collect();

    let mut template = Template::Page(PageTemplate {
        characters,
        selected_character_ids,
        plot_height: PLOT_HEIGHT,
        plot_width: PLOT_WIDTH,
    });

    if let Some(Ok("chart")) = headers.get("HX-Target").map(HeaderValue::to_str) {
        template = template.into_content();
    }

    Ok(Html(template.render()?).into_response())
}
