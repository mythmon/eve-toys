mod chart;
mod page;

pub use chart::endpoint as chart;
pub use page::endpoint as page;

pub const PLOT_WIDTH: u32 = 900;
pub const PLOT_HEIGHT: u32 = 500;
