use crate::{
    auth::CharacterInfo,
    endpoints::wallet_graph::{PLOT_HEIGHT, PLOT_WIDTH},
    error::ETError,
    eve::Api,
    extractors::MultiQueryString,
};
use anyhow::{anyhow, Context};
use askama::Template as AskamaTemplate;
use axum::{
    response::{Html, IntoResponse},
    Extension,
};
use esi::{apis::wallet_api, models::GetCharactersCharacterIdWalletJournal200Ok};
use serde::Serialize;
use std::{
    collections::{HashMap, HashSet},
    sync::Arc,
};
use time::{format_description::well_known::Iso8601, OffsetDateTime};

pub use crate::filters;

#[derive(Serialize, AskamaTemplate)]
#[template(path = "wallet_graph/chart.html")]
struct Template {
    characters: HashMap<u32, CharacterInfo>,
    running_balance_data: Vec<RunningBalanceChartDatum>,
    plot_width: u32,
    plot_height: u32,
}

#[derive(Serialize)]
struct CharacterAndJournal {
    character: CharacterInfo,
    journal: GetCharactersCharacterIdWalletJournal200Ok,
}

#[derive(Clone, Debug, Serialize)]
struct RunningBalanceChartDatum {
    balance: f64,
    character_id: u32,
    #[serde(with = "time::serde::iso8601")]
    date: OffsetDateTime,
}

impl Default for RunningBalanceChartDatum {
    fn default() -> Self {
        Self {
            balance: 0.0,
            character_id: 0,
            date: OffsetDateTime::now_utc(),
        }
    }
}

fn parse_date(s: &str) -> Result<OffsetDateTime, ETError> {
    OffsetDateTime::parse(s, &Iso8601::DEFAULT)
        .context(format!("parsing date: {s}"))
        .map_err(ETError::Deserialization)
}

pub async fn endpoint(
    Extension(api): Extension<Arc<Api>>,
    qs: MultiQueryString,
) -> Result<impl IntoResponse, ETError> {
    let all_char_auths = (*api.auth)
        .clone()
        .map(|auth_info| auth_info.chars)
        .unwrap_or_default();

    if all_char_auths.is_empty() {
        Err(ETError::Auth(anyhow!("No characters found")))?;
    }

    let selected_char_ids: HashSet<u32> = match qs.0.get("character_ids") {
        Some(values) => values
            .iter()
            .filter_map(|v| v.parse::<u32>().ok())
            .collect::<HashSet<u32>>(),
        None => vec![all_char_auths[0].info.character_id]
            .into_iter()
            .collect(),
    };

    let selected_char_auth_and_info = all_char_auths
        .iter()
        .filter(|c| selected_char_ids.contains(&c.info.character_id))
        .collect::<Vec<_>>();

    let client = reqwest::Client::new();
    let configuration = esi::apis::configuration::Configuration {
        client,
        ..Default::default()
    };
    let params = wallet_api::GetCharactersCharacterIdWalletJournalParams {
        character_id: 0,
        datasource: None,
        if_none_match: None,
        page: None,
        token: None,
    };

    let mut journal = vec![];
    for &c in &selected_char_auth_and_info {
        let configuration = esi::apis::configuration::Configuration {
            oauth_access_token: Some(c.tokens.access_token.clone()),
            ..configuration.clone()
        };
        let params = wallet_api::GetCharactersCharacterIdWalletJournalParams {
            character_id: c.info.character_id,
            ..params.clone()
        };

        let entries =
            wallet_api::get_characters_character_id_wallet_journal(&configuration, params).await?;
        journal.extend(entries.into_iter().map(|journal| CharacterAndJournal {
            character: c.info.clone(),
            journal,
        }));
    }
    journal.sort_by_key(|d| d.journal.date.clone());

    let mut cursor: HashMap<u32, RunningBalanceChartDatum> = HashMap::new();

    let mut running_balance_data = cursor.values().cloned().collect::<Vec<_>>();
    for journal_entry in journal {
        let date = parse_date(&journal_entry.journal.date)?;
        // Eve will have multiple transactions all occuring at the same time. If
        // that happens, we remove (and then replace them) the previous values
        // to avoid tiny spikes in the chart.
        // TODO it would probably be easier to directly overwrite these.
        if let Some(last_point) = cursor.values().next() {
            if date - last_point.date < time::Duration::milliseconds(1) {
                running_balance_data.truncate(running_balance_data.len() - cursor.values().count());
            }
        }

        if let Some(balance) = journal_entry.journal.balance {
            let entry = cursor
                .entry(journal_entry.character.character_id)
                .or_insert_with(|| RunningBalanceChartDatum {
                    character_id: journal_entry.character.character_id,
                    date,
                    balance,
                });
            entry.balance = balance;
        }

        for d in cursor.values_mut() {
            d.date = date;
        }
        running_balance_data.extend(cursor.values().cloned());
    }

    let template = Template {
        characters: selected_char_auth_and_info
            .iter()
            .map(|c| (c.info.character_id, c.info.clone()))
            .collect(),
        running_balance_data,
        plot_width: PLOT_WIDTH,
        plot_height: PLOT_HEIGHT,
    };
    let body: String = template.render().map_err(ETError::map)?;

    // TODO is there a better way to do this?
    let mut res = Html(body).into_response();
    res.headers_mut()
        .insert("Cache-Control", "max-age=3600".parse().unwrap());
    Ok(res)
}
