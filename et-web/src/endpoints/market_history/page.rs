use std::{fmt::Display, sync::Arc};

use crate::{auth::CharacterInfo, context::AppContext, error::ETError, eve::Api};
use anyhow::anyhow;
use askama::Template as AskamaTemplate;
use axum::{
    extract::{Query, State},
    headers::HeaderValue,
    http::HeaderMap,
    response::{Html, IntoResponse},
    Extension,
};

// for templates
pub use crate::filters;

#[derive(serde::Deserialize)]
pub struct Params {
    character_id: Option<u32>,
    search: Option<String>,
    #[serde(rename = "type")]
    transaction_type: Option<TransactionType>,
}

enum Template {
    Page(PageTemplate),
    Content(ContentTemplate),
}

impl Template {
    pub fn render(&self) -> Result<String, ETError> {
        match self {
            Self::Page(page) => page.render(),
            Self::Content(content) => content.render(),
        }
        .map_err(ETError::map)
    }

    pub fn into_content(self) -> Self {
        match self {
            Self::Page(page) => Self::Content(ContentTemplate {
                characters: page.characters,
                selected_character_id: page.selected_character_id,
                search_query: page.search_query,
                table_url: page.table_url,
                transaction_type: page.transaction_type,
            }),
            Self::Content(content) => Self::Content(content),
        }
    }
}

#[derive(AskamaTemplate)]
#[template(path = "market_history/page.html")]
struct PageTemplate {
    characters: Vec<CharacterInfo>,
    selected_character_id: u32,
    search_query: Option<String>,
    table_url: String,
    transaction_type: Option<TransactionType>,
}

#[derive(serde::Deserialize, Debug)]
pub enum TransactionType {
    #[serde(rename = "either")]
    Either,
    #[serde(rename = "buy")]
    Buy,
    #[serde(rename = "sell")]
    Sell,
}

impl Display for TransactionType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            TransactionType::Buy => write!(f, "buy"),
            TransactionType::Sell => write!(f, "sell"),
            TransactionType::Either => write!(f, "either"),
        }
    }
}

#[derive(AskamaTemplate)]
#[template(path = "market_history/content.html")]
struct ContentTemplate {
    characters: Vec<CharacterInfo>,
    selected_character_id: u32,
    search_query: Option<String>,
    table_url: String,
    transaction_type: Option<TransactionType>,
}

pub async fn endpoint(
    State(context): State<AppContext>,
    Extension(api): Extension<Arc<Api>>,
    headers: HeaderMap,
    Query(params): Query<Params>,
) -> Result<impl IntoResponse, ETError> {
    let all_char_auths = (*api.auth)
        .clone()
        .map(|auth_info| auth_info.chars)
        .unwrap_or_default();

    if all_char_auths.is_empty() {
        Err(ETError::Auth(anyhow!("No signed-in characters found")))?;
    }

    let selected_character_id = params
        .character_id
        .unwrap_or(all_char_auths[0].info.character_id);

    let characters: Vec<CharacterInfo> = all_char_auths.into_iter().map(|c| c.info).collect();

    let mut table_url = context
        .settings
        .web
        .base_url
        .join("/market-history/table")?;
    if let Some(search) = &params.search {
        table_url.query_pairs_mut().append_pair("search", search);
    }
    if let Some(selected_character_id) = params.character_id {
        table_url
            .query_pairs_mut()
            .append_pair("character_id", &selected_character_id.to_string());
    }
    if let Some(transaction_type) = &params.transaction_type {
        table_url
            .query_pairs_mut()
            .append_pair("type", &transaction_type.to_string());
    }

    let mut template = Template::Page(PageTemplate {
        characters,
        selected_character_id,
        search_query: params.search,
        table_url: table_url.to_string(),
        transaction_type: params.transaction_type,
    });
    if let Some(Ok("content")) = headers.get("HX-Target").map(HeaderValue::to_str) {
        template = template.into_content();
    }

    let body = template.render().map_err(ETError::map)?;

    Ok(Html(body).into_response())
}
