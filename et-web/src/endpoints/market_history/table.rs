use crate::{
    context::AppContext,
    endpoints::market_history::page::TransactionType,
    error::ETError,
    eve::{Api, ApiWalletTransaction},
};
use anyhow::{anyhow, Context as AnyhowContext};
use askama::Template as AskamaTemplate;
use axum::{
    extract::{Query, State},
    http::HeaderValue,
    response::{Html, IntoResponse},
    Extension,
};
use std::{
    collections::{HashMap, HashSet},
    sync::Arc,
};

pub use crate::filters;

#[derive(serde::Deserialize)]
pub struct Params {
    character_id: Option<u32>,
    search: Option<String>,
    #[serde(rename = "type")]
    transaction_type: Option<TransactionType>,
}

#[derive(AskamaTemplate)]
#[template(path = "market_history/table.html")]
struct Template {
    transactions: Vec<ApiWalletTransaction>,
    type_names: HashMap<u32, String>,
}

pub async fn endpoint(
    State(context): State<AppContext>,
    Extension(api): Extension<Arc<Api>>,
    Query(params): Query<Params>,
) -> Result<impl IntoResponse, ETError> {
    let all_char_auths = (*api.auth)
        .clone()
        .map(|auth_info| auth_info.chars)
        .unwrap_or_default();

    if all_char_auths.is_empty() {
        Err(ETError::Auth(anyhow!("No signed-in characters found")))?;
    }

    let selected_character_id = params
        .character_id
        .unwrap_or(all_char_auths[0].info.character_id);
    let selected_char_auth_and_info = all_char_auths
        .iter()
        .find(|c| c.info.character_id == selected_character_id)
        .context("Couldn't find auth info for selected char")
        .map_err(ETError::Auth)?;

    let transactions = api.wallet_transactions(selected_char_auth_and_info).await?;

    let ids = transactions
        .iter()
        .map(|t| t.type_id)
        .collect::<HashSet<_>>()
        .into_iter()
        .map(|id| id as i64)
        .collect::<Vec<_>>();
    let type_names: Vec<(i32, String)> =
        sqlx::query_as("SELECT id, name FROM sde_item_types WHERE id = ANY($1::int[])")
            .bind(&ids)
            .fetch_all(&context.pg_pool)
            .await
            .context("Fetching item names from SDE")
            .map_err(ETError::Db)?;
    let type_names = type_names
        .into_iter()
        .map(|(id, name)| (id as u32, name))
        .collect::<HashMap<_, _>>();

    let transactions = match &params.search {
        Some(q) if !q.is_empty() => {
            let q_lower = q.to_lowercase();
            let parts = q_lower.split(' ').collect::<Vec<_>>();
            transactions
                .into_iter()
                .filter(|trans| {
                    let haystack = type_names
                        .get(&trans.type_id)
                        .map_or_else(|| trans.type_id.to_string(), Clone::clone)
                        .to_lowercase();
                    parts.iter().all(|part| haystack.contains(part))
                })
                .collect::<Vec<_>>()
        }
        _ => transactions,
    };

    let transactions = match &params.transaction_type {
        Some(TransactionType::Buy) => transactions
            .into_iter()
            .filter(|t| t.is_buy)
            .collect::<Vec<_>>(),
        Some(TransactionType::Sell) => transactions
            .into_iter()
            .filter(|t| !t.is_buy)
            .collect::<Vec<_>>(),
        Some(TransactionType::Either) | None => transactions,
    };

    let template = Template {
        transactions,
        type_names,
    };
    let body = template.render().map_err(ETError::map)?;

    let mut push_url = context.settings.web.base_url.join("/market-history")?;
    if let Some(character_id) = params.character_id {
        push_url
            .query_pairs_mut()
            .append_pair("character_id", &character_id.to_string());
    }
    if let Some(search) = params.search {
        if !search.is_empty() {
            push_url.query_pairs_mut().append_pair("search", &search);
        }
    }
    if let Some(transaction_type) = params.transaction_type {
        push_url
            .query_pairs_mut()
            .append_pair("type", &transaction_type.to_string());
    }

    let mut res = Html(body).into_response();
    res.headers_mut()
        .append("HX-Push", HeaderValue::from_str(push_url.as_ref())?);
    Ok(res)
}
