mod page;
mod table;

pub use page::endpoint as page;
pub use table::endpoint as table;
