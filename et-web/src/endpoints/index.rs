use crate::{
    auth::CharacterInfo,
    error::ETError,
    eve::{self},
};
use askama::Template;
use axum::{
    response::{Html, IntoResponse},
    Extension,
};
use std::sync::Arc;

#[derive(Template)]
#[template(path = "../templates/index.html")]
struct IndexData {
    character_info: Vec<CharacterInfo>,
}

pub async fn endpoint(
    Extension(eve_api): Extension<Arc<eve::Api>>,
) -> Result<impl IntoResponse, ETError> {
    IndexData {
        character_info: eve_api.all_character_info(),
    }
    .render()
    .map(Html)
    .map_err(ETError::map)
}
