use axum_extra::extract::cookie::Key;
use base64::{engine::general_purpose::STANDARD_NO_PAD as b64, Engine};
use serde::{Deserialize, Deserializer};

pub fn deserialize<'de, D>(deserializer: D) -> Result<Key, D::Error>
where
    D: Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;
    let k: Vec<u8> = b64.decode(s).map_err(serde::de::Error::custom)?;
    Ok(Key::from(&k[..]))
}
