use std::net::SocketAddr;
use std::{env, path::PathBuf};

use axum_extra::extract::cookie::Key;
use config::Config;
use serde::Deserialize;
use url::Url;

mod cookie_key_serde;

#[derive(Clone, Deserialize)]
pub struct Settings {
    pub esi: EsiConfig,
    pub postgres: PostgresConfig,
    pub web: WebConfig,
}

#[derive(Clone, Deserialize)]
pub struct EsiConfig {
    pub client_id: String,
    pub secret_key: String,
}

#[derive(Clone, Deserialize)]
pub struct PostgresConfig {
    pub url: Url,
}

#[derive(Clone, Deserialize)]
pub struct WebConfig {
    pub base_url: Url,
    #[serde(with = "cookie_key_serde")]
    pub cookie_key: Key,
    pub listen: SocketAddr,
    pub static_dir: PathBuf,
}

impl Settings {
    pub fn load() -> Result<Self, config::ConfigError> {
        let environment = env::var("ET_ENVIRONMENT").unwrap_or_else(|_| "dev".into());

        let settings = Config::builder()
            .add_source(config::File::with_name("settings.default"))
            .add_source(config::File::with_name(&format!("settings.{environment}")).required(false))
            .add_source(config::File::with_name("settings.local").required(false))
            .add_source(
                config::Environment::with_prefix("ET")
                    .separator("__")
                    .prefix_separator("_"),
            )
            .build()?;

        settings.try_deserialize().map_err(|err| {
            config::ConfigError::Message(format!("Error deserializing settings: {err}"))
        })
    }
}
